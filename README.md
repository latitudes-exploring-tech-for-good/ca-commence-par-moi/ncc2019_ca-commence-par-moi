# Ça Commence Par Moi

+ *Thème :* environnement.
+ *Phase d'avancement actuelle :* conception.
+ *Compétences associés :* design, conception app mobile, vision produit, développement web/mobile.

##### #0 | Résultats obtenus lors de la Nuit du Code Citoyen
Définition des outils et de la stratégie du site en terme de rayonnement extérieur :
- pertinence des contenus proposés
- assistance et accompagnement dans la démarche de diffusion
- responsabilisation des acteurs engagés"

[En savoir plus](https://docs.google.com/document/d/1mPBaG4pNL7QoshlrUeZ89aNDyY6Uh2UeFAQTc8-v0TA/edit?usp=sharing).

##### #1 | Présentation de Ça Commence Par Moi
Ça Commence Par Moi propose plus de 400 actions simples pour la transition écologique et solidaire et changer le monde à notre échelle.
Les 3 impacts visés par Ça Commence Par Moi :
+ **Sensibiliser** et donner aux citoyens des répères pour qu'ils s'approprient la question environnemental et sociétale.
+ Permettre de **passer à l'action** simplement, en proposant des actions simples et accessibles.
+ **Diffuser** cette vision d'un monde où chacun.e peut changer le monde à son échelle.

En savoir plus : [cacommenceparmoi.org](http://cacommenceparmoi.org/).

Et parce qu'une vidéo vaut mieux qu'un long discours, c'est par [ici](https://www.youtube.com/watch?v=1EKxrRZ09Gs) !

##### #2 | Problématique : Favoriser la viralité des actions Ça Commence Par Moi
Une refonte du site est en cours de conception (cf ressources) afin de mieux adresser les 3 impacts visés par Ça Commence Par Moi. Dans ce cadre-là, il parait judicieux de compléter la version web par une outil allant de paire avec le site, pour favoriser le partage des actions et créer du lien au sein de la communauté Ça Commence Par Moi.

##### #3 | Le défi proposé
Concevoir une outil qui irait de paire avec le nouveau site de Ça Commence Par Moi, en proposant un accès simplifié aux actions et en mettant en avant les fonctionnalités sociales. Partage, défis, localisation, invitation... Sentez-vous libre d'utiliser toutes les ressources à votre disposition pour proposer un outil simple, intuitif et ludique !

##### #4 | Livrables
La plus grande valeur ajoutée de votre travail réside dans la phase d'idéation autour de concepts de défis et d'interactions paire à paire.
Les livrables suivants sont conseillés, à calibrer selon les avancées de l'équipe sur le hackathon :
+ Un partage de la réflexion sur le choix des différentes fonctionnalités mises en avant sur l'outil proposé.
+ Un benchmark de solutions similaires existantes, comment celles-ci s'articulent et comment elles pourraient se transposer à Ça Commence Par Moi.
+ Une maquette cliquable (Invision, Sketch, autre) de l'outil proposé.
+ Le détail des spécifications fonctionnelles et techniques ainsi qu'une estimation du temps nécessaire (si possible dans le temps imparti).
+ Une ou plusieurs fonctionnalités développées (si possible dans le temps imparti).

##### #5 | Ressources à disposition pour résoudre le défi
1/ Un [dossier partagé](https://drive.google.com/drive/folders/10wn01AGRw3r1MMUx03hcEo1A4G7bM3wL?usp=sharing) contennant notamment
+ Des entretiens effectués auprès d'utilisateurs ainsi qu'une synthèse de ceux-ci.
+ La maquette du site au format psd pour y faire des modifications si nécessaire.
+ L'état des réflexions actuelles sur la refonte du site.
+ Les spécifications fonctionnelles et techniques du site.

2/ La [maquette cliquable](https://projects.invisionapp.com/share/D5PPQFORMYQ#/screens/337942707). Avec notamment des commentaires qui visent à synthétiser les retours d'utilisateurs.

##### #6 | Code de conduite et philosophie du hackathon
Lors de la conception du hackathon, Latitudes a voulu prendre des partis-pris afin de rendre celui-ci particulier. 

Il s’agit d’éléments que nous souhaitons incarner collectivement avec vous tout au long des 24h :
+ La force du collectif pour faire émerger des solutions adaptées aux porteur.se.s de projets, notamment via la session de peer-learning ;
+ Une attention portée à la conception rapide, au test et à l’itération qui doivent permettre aux porteur.se.s de projets d’avoir accès aux solutions les plus abouties possibles ;
+ Le transfert de méthodes et postures, qui pourront être appliquées et transmises aux équipes par les animateur.trice.s, les mentors ET les autres équipes ;
+ L’engagement sur des solutions ouvertes, facilement utilisables par les porteur.se.s de projets, et qui vous permettront de continuer à contribuer à l’issue du hackathon si vous le souhaitez, ou de permettre à d’autres personnes de le faire.

##### #7 | Points de contact lors du hackathon
+ Thomas et Félix : bénévoles Ça Commence Par Moi et porteurs de projets.
+ Julien : fondateur de Ça Commence Par Moi.
+ Yannick : co-fondateur de Latitudes, et en charge de la préparation du défi Ça Commence Par Moi.